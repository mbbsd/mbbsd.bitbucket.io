Função Exponencial
==================

Das funções que estudamos neste curso, exponencial e logarítmica são, provavelmente,
as que maior impacto têm sobre a vida cotidiana.

Até aqui fomos expostos a funções que envolvem potências como :math:`x^2` ou
:math:`x^{2/3}` em que a base, :math:`x`, varia enquanto o expoente segue fixado
durante todo o tempo. Neste capítulo, vamos estudar funções do tipo :math:`f(x)=b^{x}`
em que a base, :math:`b`, é constante e o expoente, :math:`x`, é variável. Iniciaremos
nossos estudos com a função :math:`f(x)=2^{x}` pois esta parece ser a maneira mais
intuitiva de se começar.

Comecemos por criar uma tabela com as coordenadas de alguns dos pontos sobre o
gráfico de :math:`f(x)=2^{x}`, por marcá-los sobre o plano cartesiano e, então,
por conectá-los de modo a atender à nossa intuição.

.. math::
   \begin{array}{r|l}
   x & f(x)=2^{x} \\
   \hline
   -3 & 1/8 \\
   -2 & 1/4 \\
   -1 & 1/2 \\
    0 & 1 \\
    1 & 2 \\
    2 & 4 \\
    3 & 8 \\
   \end{array}

Alguns comentários acerca do gráfico que acabamos de construir para a função
:math:`f(x)=2^{x}` devem ser feitos. À medida que :math:`x` tende a menos infinito,
:math:`x\to-\infty` em símbolos, e assume valores cada vez menores, como :math:`x=-1000`
ou :math:`x=-1000000`, a função :math:`f(x)=2^{x}` segue produzindo valores tão pequenos
quanto :math:`f(-1000)=2^{-1000}=\frac{1}{2^{1000}}` ou :math:`f(-1000000)=2^{-1000000}=\frac{1}{2^{1000000}}`.
Então, de modo geral, tem-se que:

.. math::
   2^{x}\approx\frac{1}{\mbox{número muito grade }(+)}
   \approx\mbox{número muito pequeno }(+)

à medida que :math:`x\to-\infty`. Assim é que se diz que :math:`2^{x}\to0^{+}` à
medida que :math:`x\to-\infty`. Ao contrário, à medida que :math:`x\to\infty`, a
variável :math:`x` assume valores cada vez maiores, como :math:`x=1000000` ou
:math:`x=1000000000`, e a função :math:`f(x)=2^{x}` produz valores tão grandes
quanto :math:`f(1000000)=2^{1000000}` ou mesmo :math:`f(1000000000)=2^{1000000000}`
e, assim, é que se tem :math:`2^{x}\to\infty` no caso em que :math:`x\to\infty`.
Como resultado, nosso gráfico sugere que a imagem de :math:`f(x)=2^{x}` é o intervalo
:math:`(0,\infty)`. Além disso, cada reta horizontal intersecta o gráfico de
:math:`f(x)` não mais do que uma vez, o que mostra que :math:`f(x)` é injetiva
e, portanto, inversível. Note que, *ao ligarmos os pontos de maneira intuitiva*,
nós tacitamente assumimos que :math:`f(x)` é uma função **contínua** definida
para todo número real. Em particular, nós sugerimos que coisas como :math:`2^{\sqrt{2}}`
existem enquanto números reais. Neste ponto nós devemos, ainda que de maneira vaga,
tentar dar um sentido a :math:`2^{\sqrt{2}}`. O número :math:`\sqrt{2}` é irracional e
como tal, sua representação decimal não tem um período ou um fim.  Podemos, todavia,
aproximar :math:`\sqrt{2}` por números racionais e, então, utilizá-los para aproximar
:math:`2^{\sqrt{2}}`. Por exemplo, podemos aproximar :math:`\sqrt{2}` por :math:`1.41`
para, então, aproximar :math:`2^{\sqrt{2}}` por :math:`2^{1.41}=2^{141/100}=\sqrt[100]{2^{141}}`.
Este pode não ser um número fácil de se manipular, mas ao menos é um número que
podemos compreender em termos de potências e raízes. É intuitivo que sucessivas
aproximações de :math:`\sqrt{2}` conduzem a aproximações cada vez melhores de :math:`2^{\sqrt{2}}`,
de modo que :math:`2^{\sqrt{2}}` é o resultado desta sequência de aproximações. Nós
remetemos o leitor interessado a cursos de Cálculo Diferencial e Integral para uma
explicação mais rigorosa e detalhada dos fatos acima.

.. hint::
   Seja :math:`b>0`, :math:`b\neq1`, uma constante. Uma função da forma :math:`f(x)=b^{x}`
   é chamada de função exponencial. A constante :math:`b` é dita a base da exponencial.

Propriedades
------------

Seja :math:`f(x)` a função :math:`f(x)=b^{x}`, em que :math:`b>0`,
:math:`b\neq1`, é uma constante. Assim, são propriedades de :math:`f(x)`:

1. O domínio de :math:`f(x)=b^{x}` é o conjunto :math:`\mathbb{R}`, de todos os
   números reais;

2. O contradomínio de :math:`f(x)=b^{x}` é, também, igual a :math:`\mathbb{R}`;

3. A imagem de :math:`f(x)=b^{x}`, isto é, o conjunto

   .. math::
      \mbox{Im}(f)=\{y\in\mathbb{R}:\exists\,\,x\in\mathbb{R},\,y=b^{x}\},

   é o conjunto :math:`(0,\infty)=\{x\in\mathbb{R}:x>0\}`;

4. :math:`f(x)=b^{x}` é, para :math:`b>1`, uma função estritamente crescente,
   isto é,

   .. math::
      \forall\,x,y\in\mathbb{R}:\quad x<y\implies b^{x}<b^{y}.

   Ao contrário, caso se tenha :math:`0<b<1`, então :math:`f(x)=b^{x}` é
   estritamente decrescente, ou seja,

   .. math::
      \forall\,x,y\in\mathbb{R}:\quad x<y\implies b^{x}>b^{y}.

   Em qualquer caso, portanto, :math:`f(x)=b^{x}` é uma função injetora e, como
   tal, é, quando restrita à sua imagem, inversível;

5. tem-se que:

   .. math::
      f(0)=1\iff b^{0}=1;

6. Quaisquer que sejam :math:`x,y\in\mathbb{R}`, tem-se que:

   .. math::
      f(x+y)=f(x)\cdot f(y)\iff b^{x+y}=b^{x}\cdot b^{y}.

