ORIG = ${HOME}/projects/notes/build/html
DEST = ${HOME}/projects/mbbsd_bitbucket_io

up:
	rm -rf course
	rm genindex.html
	rm -rf _images
	rm index.html
	rm objects.inv
	rm search.html
	rm searchindex.js
	rm -rf _sources
	rm -rf _static
	cp -r "$(ORIG)"/* "$(DEST)"
	git add .
	git commit -m "made with <3"
	git push

.PHONY: up
